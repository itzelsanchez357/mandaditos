package com.example.proyectomandaditos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class PedidosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);

        Button btnAceptarPedido =(Button) findViewById(R.id.btnAceptarPedido);
        FloatingActionButton btnInicio = (FloatingActionButton) findViewById(R.id.fBtnInicioPe);
        FloatingActionButton btnPerfil = (FloatingActionButton) findViewById(R.id.fBtnPerfilPe);
        FloatingActionButton btnSeguimiento = (FloatingActionButton) findViewById(R.id.fBtnSeguimientoPe);
        FloatingActionButton btnPedido = (FloatingActionButton) findViewById(R.id.fBtnPedidoPe);

        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),PrincipalActivity.class);
                startActivity(i);
            }
        });
        btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),MiPerfilActivity.class);
                startActivity(i);
            }
        });
        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),PedidosActivity.class);
                startActivity(i);
            }
        });
        btnSeguimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),SeguimientoActivity.class);
                startActivity(i);
            }
        });

        btnAceptarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),DetallePedidoActivity.class);
                startActivity(i);
            }
        });


    }
}