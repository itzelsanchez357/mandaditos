package com.example.proyectomandaditos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MiPerfilActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_perfil);

        Button btnGuardarPerfil = (Button) findViewById(R.id.btnGuardarPerfil);
        Button btnEditarFoto = (Button) findViewById(R.id.btn_edit_img2);
        Button btnEditarNombre = (Button) findViewById(R.id.btn_edit_nom);
        Button btnEditarApellidos = (Button) findViewById(R.id.btn_edit_apellido);
        Button btnEditarTelefono = (Button) findViewById(R.id.btn_edit_tel);
        Button btnEditarCorreo = (Button) findViewById(R.id.btn_edit_correo);
        Button btnEditarContra = (Button) findViewById(R.id.btn_edit_contra);

        FloatingActionButton btnInicio = (FloatingActionButton) findViewById(R.id.fBtnInicioM);
        FloatingActionButton btnPerfil = (FloatingActionButton) findViewById(R.id.fBtnPerfilM);
        FloatingActionButton btnSeguimiento = (FloatingActionButton) findViewById(R.id.fBtnSeguimientoM);
        FloatingActionButton btnPedido = (FloatingActionButton) findViewById(R.id.fBtnPedidoM);

        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),PrincipalActivity.class);
                startActivity(i);
            }
        });
        btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),MiPerfilActivity.class);
                startActivity(i);
            }
        });
        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),PedidosActivity.class);
                startActivity(i);
            }
        });
        btnSeguimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),SeguimientoActivity.class);
                startActivity(i);
            }
        });


        btnGuardarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),PrincipalActivity.class);
                startActivity(i);
            }
        });
        btnEditarNombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnEditarApellidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnEditarTelefono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnEditarCorreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnEditarContra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}