package com.example.proyectomandaditos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class PrincipalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        FloatingActionButton btnInicio = (FloatingActionButton) findViewById(R.id.fBtnInicio);
        FloatingActionButton btnPerfil = (FloatingActionButton) findViewById(R.id.fBtnPerfil);
        FloatingActionButton btnSeguimiento = (FloatingActionButton) findViewById(R.id.fBtnSeguimiento);
        FloatingActionButton btnPedido = (FloatingActionButton) findViewById(R.id.fBtnPedido);

        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),PrincipalActivity.class);
                startActivity(i);
            }
        });
        btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),MiPerfilActivity.class);
                startActivity(i);
            }
        });
        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),PedidosActivity.class);
                startActivity(i);
            }
        });
        btnSeguimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),SeguimientoActivity.class);
                startActivity(i);
            }
        });
    }
}